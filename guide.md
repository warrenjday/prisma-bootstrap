1. install docker and docker-compose
2. `npm i prisma@latest -g`
3. `prisma init`
4. change localhost to "docker.for.mac.localhost"
4. `docker-compose up`
5. new window: `prisma deploy`
6. `prisma playground`


# Create queries

```
mutation {
  createUser(data: { name: "Warren" }) {
    id
  }
}
```

```
mutation {
  createVehicle(data: {
    name: "Ford",
    owner: { connect: { id: "cjjird1qr000i0787l0xedlgb" } }
  }) {
    name
  }
}
```